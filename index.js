var express = require('express');
var Web3 = require('web3');
var solc = require('solc');
var fs = require('fs');
var account = "0x865236E83a507Ac1aCA91b7Bd3201549915537A5";
var app = express();
var contractInstance;
web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

app.get('/', function(req,res){

  res.render('mainPage.ejs');

});

app.listen(8080);

app.get('/mainInfo/:ethereumAddress/:password', function(req, res) {
  initialize();

  contractInstance.register(req.params.ethereumAddress, {from: account, gas:500000});
  web3.personal.unlockAccount(req.params.ethereumAddress, req.params.password);
  var userBalance = contractInstance.balanceOf(req.params.ethereumAddress);
  var totalParticipants = contractInstance.numRegistrants.call();
  var eventQuota = contractInstance.quota.call();
  var etherBalance = web3.eth.getBalance(req.params.ethereumAddress);
  res.render('index.ejs', { userAddress: req.params.ethereumAddress, etherBalance: etherBalance, userBalance: userBalance, totalParticipants:totalParticipants, eventQuota:eventQuota });
});

app.get('/registerUser/:password', function(req,res){
  initialize();

  var newUserAddress = web3.personal.newAccount(req.params.password);
  web3.personal.unlockAccount(newUserAddress, req.params.password);
  contractInstance.register(newUserAddress, {from: account, gas:500000});
  var userBalance = contractInstance.balanceOf(newUserAddress);
  var totalParticipants = contractInstance.numRegistrants.call();
  var eventQuota = contractInstance.quota.call();
  var etherBalance = web3.eth.getBalance(newUserAddress);
  res.render('index.ejs', { userAddress: newUserAddress, etherBalance: etherBalance, userBalance: userBalance, totalParticipants:totalParticipants, eventQuota:eventQuota });
});

app.get('/mainInfo/buyStandartTicket/:ethereumAddress/:password', function(req,res){

  web3.personal.unlockAccount(req.params.ethereumAddress, req.params.password);
  contractInstance.buyStandartTicket(req.params.ethereumAddress, {from:req.params.ethereumAddress, gas:500000});
  var userBalance = contractInstance.balanceOf(req.params.ethereumAddress);
  var totalParticipants = contractInstance.numRegistrants.call();
  var eventQuota = contractInstance.quota.call();
  var etherBalance = web3.eth.getBalance(req.params.ethereumAddress);
  res.render('index.ejs', { userAddress: req.params.ethereumAddress, etherBalance: etherBalance, userBalance: userBalance, totalParticipants:totalParticipants, eventQuota:eventQuota });
});

app.get('/mainInfo/buyVIPTicket/:ethereumAddress/:password', function(req,res){

  web3.personal.unlockAccount(req.params.ethereumAddress, req.params.password);
  contractInstance.buyVIPTicket(req.params.ethereumAddress, {from:req.params.ethereumAddress, gas:500000});
  var userBalance = contractInstance.balanceOf(req.params.ethereumAddress);
  var totalParticipants = contractInstance.numRegistrants.call();
  var eventQuota = contractInstance.quota.call();
  var etherBalance = web3.eth.getBalance(req.params.ethereumAddress);
  res.render('index.ejs', { userAddress: req.params.ethereumAddress, etherBalance: etherBalance, userBalance: userBalance, totalParticipants:totalParticipants, eventQuota:eventQuota });
});

app.get('/mainInfo/buyCHRTokens/:buyAmount/:ethereumAddress/:password', function(req,res){

  web3.personal.unlockAccount(req.params.ethereumAddress, req.params.password);
  contractInstance.buyTokens(req.params.buyAmount, req.params.ethereumAddress, {from: req.params.ethereumAddress, gas: 500000});
  var userBalance = contractInstance.balanceOf(req.params.ethereumAddress);
  var totalParticipants = contractInstance.numRegistrants.call();
  var eventQuota = contractInstance.quota.call();
  var etherBalance = web3.eth.getBalance(req.params.ethereumAddress);
  res.render('index.ejs', { userAddress: req.params.ethereumAddress, etherBalance: etherBalance, userBalance: userBalance, totalParticipants:totalParticipants, eventQuota:eventQuota });
});

app.get('/mainInfo/sellCHRTokens/:sellAmount/:ethereumAddress/:password', function(req,res){

  web3.personal.unlockAccount(req.params.ethereumAddress, req.params.password);
  contractInstance.sellTokens(req.params.buyAmount, req.params.ethereumAddress, {from: req.params.ethereumAddress, gas:500000});
  var userBalance = contractInstance.balanceOf(req.params.ethereumAddress);
  var totalParticipants = contractInstance.numRegistrants.call();
  var eventQuota = contractInstance.quota.call();
  var etherBalance = web3.eth.getBalance(req.params.ethereumAddress);
  res.render('index.ejs', { userAddress: req.params.ethereumAddress, etherBalance: etherBalance, userBalance: userBalance, totalParticipants:totalParticipants, eventQuota:eventQuota });
});

function initialize(){

  web3.personal.unlockAccount("0x865236E83a507Ac1aCA91b7Bd3201549915537A5", "password123456");
  var sourceCode = fs.readFileSync('./Organization.sol','utf8')
  var compiled = solc.compile(sourceCode);
  var abiDefinition = JSON.parse(compiled.contracts[":Organization"].interface);
  var MyContract = web3.eth.contract(abiDefinition);
  var byteCode = compiled.contracts[':Organization'].bytecode;
  var address = "0xBfA8B7910B9c3f9BA4c2B437450E6DE5a87fA406";
  contractInstance = MyContract.at(address);

}
