pragma solidity ^0.4.0;

contract Organization {  // can be killed, so the owner gets sent the money in the end
    
    mapping (address => uint) public registrantsPaid;
    uint public vipParticipants;
    uint public standartParticipants;
    uint public numRegistrants;
    uint public quota;
    uint public bankBalance = 10000;
    string coinName;
    string coinSymbol;
    uint amount;
    uint totalWeiAmount;
    uint buyCHRtokens = 500;
    uint sellCHRtokens = 300;
    uint public totalAmount;
    mapping (address => uint256) public balanceOf;
    mapping (address => uint256) public registeredAlready;
    address organizer;
    
    event Transfer(address indexed from, address indexed to, uint256 value);
    event BuyerAdress(address);
    
    function Organization(uint256 initialSupply, string tokenName, string tokenSymbol) public{
        organizer = msg.sender;
        quota = 1000;
        numRegistrants = 0;
        vipParticipants = 0;
        totalAmount = 0;
        coinName = tokenName;
        coinSymbol = tokenSymbol;
        bankBalance = initialSupply; 
    }
    
    /* Send coins */
    function register(address _to) public payable{
        require(registeredAlready[_to] <= 0);
        require(bankBalance >= 200);            // Check if the sender has enough
        bankBalance -= 200;                     // Subtract from the sender
        balanceOf[_to] += 200;                  // Add the same to the recipient
        registeredAlready[_to] = 200;
    }
    
    function changeQuota(uint newquota) public {
        quota = newquota;
    }
    
    function buyTokens(uint weiAmount, address tokenBuyer) public payable{
        totalWeiAmount += weiAmount;
        amount = weiAmount / buyCHRtokens;                    // calculates the amount
        require(bankBalance >= amount);               // checks if it has enough to sell
        balanceOf[tokenBuyer] += amount;                  // adds the amount to buyer's balance
        bankBalance -= amount;                        // subtracts amount from seller's balance
        Transfer(this, tokenBuyer, amount);               // execute an event reflecting the change                                // ends function and returns
    }
    
    function sellTokens(uint CHRamount, address tokenSeller) public returns (uint revenue){
        require(balanceOf[tokenSeller] > CHRamount);
        bankBalance += CHRamount;
        balanceOf[tokenSeller] -= CHRamount;
        revenue = CHRamount * sellCHRtokens;
        //Send ether to User (must succeed to go further)
        Transfer(tokenSeller, this, CHRamount);
        return revenue;
    }
    
    function buyVIPTicket(address buyerAdress) public payable {
        if (numRegistrants >= quota) { 
          return; // throw ensures funds will be returned
        }
        if (balanceOf[buyerAdress] < 160){
            return; //if the buyer doesn't have enough money will be returned.
        }
        
        registrantsPaid[buyerAdress] += 160;
        BuyerAdress(buyerAdress);
        numRegistrants++;
        vipParticipants++;
        balanceOf[buyerAdress] -= 160;
        totalAmount += 160;
    }
    
    function buyStandartTicket(address buyerAdress) public payable {
        if (numRegistrants >= quota) { 
          return; // throw ensures funds will be returned
        }
        if (balanceOf[buyerAdress] < 100){
            return; //if the buyer doesn't have enough money will be returned.
        }
        
        registrantsPaid[buyerAdress] += 100;
        BuyerAdress(buyerAdress);
        numRegistrants++;
        standartParticipants++;
        balanceOf[buyerAdress] -= 100;
        totalAmount += 100;
    }
    
    function transfer() public{
        if(totalAmount >0){
            bankBalance += totalAmount;
            totalAmount = 0;
        }
    }
    
    function destroy() public{
        require(msg.sender == organizer);
        bankBalance += totalAmount;
        selfdestruct(msg.sender);
        
    }
}